
public abstract class Square extends Shape implements TwoDimensionalShapeInterface {
	
	private int side;
	private String square_color;
	
	public Square(int side, String square_color)
	{
		this.side = side;
		this.square_color = square_color;
	}

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}

	public String getSquare_color() {
		return square_color;
	}

	public void setSquare_color(String square_color) {
		this.square_color = square_color;
	}
	
	public double calculateArea()
	{
		int sq_area = side * side;
		System.out.println("The area is " );
		return sq_area;
	}

	public void printInfo()
	{
		
		System.out.println("The side is" +side);
		System.out.println("The square_color is" +square_color);
		
	}
	
}


public abstract class Triangle extends Shape implements TwoDimensionalShapeInterface{

	private int base;
	private int height;
	private String triangle_color;
	
	public Triangle(int base, int height, String triangle_color)
	{
		this.base = base;
		this.height = height;
		this.triangle_color = triangle_color;
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getTriangle_color() {
		return triangle_color;
	}

	public void setTriangle_color(String triangle_color) {
		this.triangle_color = triangle_color;
	}
	
	public double calculateArea()
	{
		double tri_area = 1/2 * base * height;
		System.out.println("The area of a triangle is  ");
		return tri_area;
		
	}
	public void printInfo()
	{
		System.out.println("The base is" +base);
		System.out.println("The height is" +height);
		System.out.println("The triangle_color is" +triangle_color);
		
	}
	
	
}
